import math


class LoadStrategy:

    def __init__(self, start=0, min=0, max=None, group_max=3):
        if start >= max or start < min:
            raise IndexError
        self.start = start
        self.min = min
        self.max = max
        self.group_max = group_max
        self._forward_loaded = 0
        self._backward_loaded = 1

    def __str__(self):
        def display(index):
            if self._next_backward_pos() < index < self._next_forward_pos():
                if index == self.start:
                    return 'o'
                else:
                    return 'x'
            else:
                if index == self.start:
                    return '*'
                else:
                    return '-'

        return ''.join([display(i) for i in range(self.min, self.max)])

    @property
    def nb_loaded(self):
        return self._forward_loaded + self._backward_loaded

    def loaded(self):
        return [self._next_backward_pos() < index < self._next_forward_pos() for index in range(self.min, self.max)]

    def _group_size(self):
        # TODO: Account for resets that don't yield a new object:
        # group size needs to be smaller again to enable
        # fast loading around current pointer.
        if self.nb_loaded <= 2:
            return 1
        return min(
            math.ceil(math.log(self.nb_loaded - 1, 3)),
            self.group_max
        )

    def _difference(self):
        return 2 * self._backward_loaded - self._forward_loaded

    def _next_forward_pos(self):
        return self.start + self._forward_loaded

    def _next_backward_pos(self):
        return self.start - self._backward_loaded

    def _forward_items_left(self):
        # TODO: To investigate: the <= might have to be a <
        return self._next_forward_pos() <= self.max

    def _backward_items_left(self):
        return self._next_backward_pos() >= self.min

    def _get_range(self, a, b):
        first = min(a, b)
        first = max(first, self.min)
        last = max(a, b)
        last = min(last, self.max)
        return list(range(first, last))

    def __iter__(self):
        return self

    def __next__(self):
        if self._forward_items_left() or self._backward_items_left():
            diff = self._difference()
            forward = (diff >= 0)
            size = self._group_size()

            # TODO: Improve the conditional statements to avoid empty arrays
            if (not forward) and self._backward_items_left():
                size = min(size, abs(diff))
                pos = self._next_backward_pos()
                self._backward_loaded += size
                return self._get_range(pos - size + 1, pos + 1)
            else:
                pos = self._next_forward_pos()
                self._forward_loaded += size
                return self._get_range(pos, pos + size)
        else:
            raise StopIteration

    def sacrifice(self):
        # TODO: To allow for garbage collection: implement a sacrifice priority
        return []

    def reset(self, start):
        new = LoadStrategy(start, self.min, self.max, self._group_size())
        if self._next_forward_pos() >= start > self._next_backward_pos():
            delta = start - self.start
            new._forward_loaded = max(self._forward_loaded - delta, 0)
            new._backward_loaded = max(self._backward_loaded + delta, 1)
        return new
