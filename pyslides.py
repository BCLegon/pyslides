import logging
import os
import tempfile
import threading

from PIL import Image
from PyPDF2 import PdfFileReader
from pdf2image import convert_from_path

from strategy import LoadStrategy


class SlideShow:

    def __init__(self, pdf_path, size=1920):
        self.path = pdf_path
        self.size = size
        self.current_index = None
        with open(pdf_path, 'rb') as file:
            self.nb_pages = PdfFileReader(file).getNumPages()
        self._frames = [None] * self.nb_pages
        self._frames_lock = threading.Lock()
        self._strategy = None
        self._strategy_lock = threading.Lock()
        self._temp_dir = None
        self._temp_dir_lock = threading.Lock()
        self._threads = []

    def start(self, index=0):
        self.current_index = index
        self.go_to(index)

        # TODO: To reduce memory usage: turn this into a separate master thread that
        # starts daemons as needed, so frame loading can resume after pausing.
        # To increase loading speed: automatically tune self._strategy.group_max
        for _ in range(4):
            thread = threading.Thread(target=self._frame_load_daemon, daemon=True)
            self._threads.append(thread)
            thread.start()

    def _frame_load_daemon(self):
        try:
            while True:
                with self._strategy_lock:
                    logging.info(str(self._strategy))
                    batch = next(self._strategy)
                logging.info(batch)
                if len(batch) > 0:
                    self._load_frames(batch[0], batch[-1] + 1)
                if self._pause_loading():
                    break
        except StopIteration:
            pass

    def _pause_loading(self):
        # TODO: To reduce memory usage: implement this with a buffer_size based stop condition
        return False

    def buffered_frames(self):
        with self._strategy_lock:
            buffered = self._strategy.loaded()
        return buffered

    def buffer_size(self):
        with self._frames_lock:
            frame_paths = self._frames
        return sum(os.path.getsize(path) for path in frame_paths if path is not None)

    def go_to(self, index):
        if self.current_index is None:
            raise RuntimeError('Slide show has not been started. Start the slide show by calling the start method.')

        if index < 0 or index >= self.nb_pages:
            raise IndexError
        self.current_index = index

        thread = threading.Thread(target=self._reset_strategy, args=(index,), daemon=True)
        thread.start()

    def _reset_strategy(self, start):
        with self._strategy_lock:
            if self._strategy is None:
                self._strategy = LoadStrategy(start=start, max=self.nb_pages)
            else:
                self._strategy = self._strategy.reset(start)

    def current_frame(self, offset=0):
        if self.current_index is None:
            raise RuntimeError('Slide show has not been started. Start the slide show by calling the start method.')
        index = self.current_index + offset
        frame_path = self._load_frame(index)
        return Image.open(frame_path)

    def _load_frame(self, index):
        with self._frames_lock:
            frame_path = self._frames[index]

        if frame_path is None:
            frame_path = self._get_image_paths(index + 1, index + 1)[0]
            with self._frames_lock:
                self._frames[index] = frame_path

        return frame_path

    def _load_frames(self, begin, end):
        with self._frames_lock:
            frame_paths = self._frames[begin:end]

        if frame_paths.count(None) == len(frame_paths):
            frame_paths = self._get_image_paths(begin + 1, end)
            with self._frames_lock:
                self._frames[begin:end] = frame_paths
        elif frame_paths.count(None) != 0:
            frame_paths = [self._load_frame(index) for index in range(begin, end)]

        return frame_paths

    def _unload_frame(self, index):
        # TODO: Test this method
        with self._frames_lock:
            frame_path = self._frames[index]

        if frame_path is not None:
            os.remove(frame_path)
            with self._frames_lock:
                self._frames[index] = None

    @property
    def _temp_dir_name(self):
        with self._temp_dir_lock:
            if self._temp_dir is None:
                self._temp_dir = tempfile.TemporaryDirectory()
            name = self._temp_dir.name
        return name

    @staticmethod
    def get_images_from_pdf(file_path, first_page=None, last_page=None, size=1920):
        with tempfile.TemporaryDirectory() as folder:
            image_paths = convert_from_path(file_path, output_folder=folder,
                                            first_page=first_page, last_page=last_page,
                                            fmt='ppm', thread_count=4, paths_only=True, size=size)
            for path in image_paths:
                yield Image.open(path)

    def _get_image_paths(self, first_page=None, last_page=None):
        image_paths = convert_from_path(self.path, output_folder=self._temp_dir_name,
                                        first_page=first_page, last_page=last_page,
                                        fmt='ppm', thread_count=4, paths_only=True, size=self.size)
        return image_paths

    def __iter__(self):
        return self

    def __next__(self):
        try:
            if self.current_index is None:
                self.start()
            else:
                self.go_to(self.current_index + 1)
            return self.current_frame()
        except IndexError:
            raise StopIteration
